libcoat-persistent-perl (0.223-2) UNRELEASED; urgency=low

  [ gregor herrmann ]
  * debian/rules: switch order of arguments to dh.

  [ Ansgar Burchardt ]
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Damyan Ivanov ]
  * pod-error.patch forwarded upstream
  * whatis-entries.patch forwarded upstream

 -- gregor herrmann <gregoa@debian.org>  Wed, 28 Jul 2010 14:32:20 -0400

libcoat-persistent-perl (0.223-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release (closes: #562384)
    + Add support for undefined primary_keys (RT#47772)
    + Make it possible to bypass DBIx::Sequence (RT#47773)
  * Removed Alexis Sukrieh from Uploaders (Closes: #536128)
  * Standards-Version 3.8.3 (no changes)
  * Refreshed patches
  * Update description

  [ gregor herrmann ]
  * Refresh patches.
  * Add more spelling fixes to pod-error.patch.
  * Update debian/coypright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 24 Jan 2010 18:32:23 +0100

libcoat-persistent-perl (0.210-1) unstable; urgency=low

  * New upstream release
    + (build-)depends on libclass-date-perl, libcoat-perl (>= 0.334)
  * New patch: whatis-entries.patch (adds missing whatis entry).
  * Bump Standards Version to 3.8.2 (no changes).

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 20 Jun 2009 11:04:53 +0200

libcoat-persistent-perl (0.104-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Fix Suggests for libcache-fastmmap-perl
  * New upstream release.
  * Bump Standards Version to 3.8.1 (no changes)
  * Use minimal debian/rules
    + Depend on debhelper (>= 7.0.8), quilt (>= 0.46-7) for dh --with quilt
  * Refresh pod-error.patch and add description.

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * Fix typo in debian/control's long description and in the POD (via the
    existing patch).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 06 Jun 2009 18:47:31 +0200

libcoat-persistent-perl (0.102-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.

  [ gregor herrmann ]
  * Refresh patch pod-error.patch.

 -- Ansgar Burchardt <ansgar@43-1.org>  Wed, 15 Oct 2008 18:35:01 +0200

libcoat-persistent-perl (0.101-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.
    + drop Build-Dep on libtest-exception-perl
  * Refresh debian/rules for debhelper 7
  * debian/control: Fix Homepage field
  * Convert debian/copyright to proposed machine-readable format
  * Add myself to Uploaders
  * Change pod-error.patch a bit

  [ gregor herrmann ]
  * Make debian/README.source a bit more verbose.

 -- Ansgar Burchardt <ansgar@43-1.org>  Fri, 19 Sep 2008 14:11:07 +0200

libcoat-persistent-perl (0.100-2) unstable; urgency=medium

  * Make dependency on libdbd-csv-perl versioned (>= 0.2200-5); earlier
    versions of libdbd-csv-perl had a wrong dependency which made
    libcoat-persistent-perl fail to build; closes: #487053.
  * Set urgency to medium, as this release closes an RC bug.
  * Set Standards-Version to 3.8.0; add debian/README.source to document quilt
    usage.

 -- gregor herrmann <gregoa@debian.org>  Thu, 19 Jun 2008 18:43:57 +0200

libcoat-persistent-perl (0.100-1) unstable; urgency=low

  * New upstream release.
  * Refresh patch pod-error.patch.

 -- gregor herrmann <gregoa@debian.org>  Tue, 03 Jun 2008 19:47:54 +0200

libcoat-persistent-perl (0.9-6-2) unstable; urgency=low

  * debian/control:
    - make (build) dependency on libcoat-perl versioned; earlier versions
      were broken under perl 5.10 because of the unusal version number
      (closes: #479942)
    - change my email address
  * Refresh debian/rules, no functional changes.
  * Add patch pod-error.patch to fix a small glitch in the POD.

 -- gregor herrmann <gregoa@debian.org>  Mon, 12 May 2008 15:48:30 +0200

libcoat-persistent-perl (0.9-6-1) unstable; urgency=low

  [ Alexis Sukrieh ]
  * New upstream release (CPAN #0.9_6)

 -- Alexis Sukrieh <sukria@debian.org>  Mon, 03 Mar 2008 15:37:33 +0100

libcoat-persistent-perl (0.0-0.5-2) unstable; urgency=low

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza).
  * Add debian/watch.
  * Add build dependency on libdbd-csv-perl (closes: #467599).
  * Add libtest-exception-perl and libcache-fastmmap-perl to
    Build-Depends-Indep.
  * debian/rules: Remove unused dh_installexamples.
  * debian/copyright: fix upstream source location.
  * Add /me to Uploaders.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Tue, 26 Feb 2008 16:40:15 +0100

libcoat-persistent-perl (0.0-0.5-1) unstable; urgency=low

  * Initial Release. (Closes: #467208)

 -- Alexis Sukrieh <sukria@debian.org>  Sun, 24 Feb 2008 00:08:05 +0100
